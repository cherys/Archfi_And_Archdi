# archfi & archdi

首先明确指出这是一个FORK的项目，我只是很喜欢而已。尊重作者的劳动成果。

非常感谢原作者的辛勤付出、
原作者为：MatMoul
原地址为：https://github.com/MatMoul/archfi
          
         https://github.com/MatMoul/archdi

--------------原文如下：----------------

Just a simple bash script wizard to install Arch Linux after you have booted on the official Arch Linux install media.

With this script, you can install Arch Linux with two simple terminal commands.

This wizard is made to install minimum packages (Base, GRUB, and optionally efibootmgr).<br />
At the end of this wizard, you can install or launch [archdi](https://github.com/MatMoul/archdi) (Arch Linux Destop Install) to install and configure desktop packages.<br />

You can watch my videos to see how to use it [here](https://www.youtube.com/playlist?list=PLytHgIKLV1caHlCrcTSkm5OF2WSVI1_Sq).

### How to use:<br />
Boot with the Arch Linux image found [here](https://www.archlinux.org/download/).

Download the script with:
```
wget archfi.sf.net/archfi
```
from the command line. If SourceForge is down, use this instead:<br />
```
wget matmoul.github.io/archfi
```

And launch the script with:<br />
```
sh archfi
```
then follow the on-screen instructions to completion.

If you require extra help, visit the provided video playlist and follow my example.


----------------archdi-----------
archdi

Just a simple bash script wizard to install and configure a full personal computer with Arch Linux.

First, install Arch Linux with the official media and optionally with archfi.
Look at archfi project to start your arch linux installation : https://github.com/MatMoul/archfi

After reboot and your network ready, download the script :
curl -L archdi.sf.net/archdi > archdi
or if sourceforge is down :
```
curl -L matmoul.github.io/archdi > archdi
```

And run the script :
```
sh archdi
```
Or you can install it on your system with
```
sh archdi -i
```
And then you can use archdi cmd.
With this, you can make your updates or install your packages later.

You can watch my videos to show how to use it :
https://www.youtube.com/playlist?list=PLytHgIKLV1caHlCrcTSkm5OF2WSVI1_Sq

This package is only a launcher for archdi-pkg.
If you want ask for new features, open an issue on https://github.com/MatMoul/archdi-pkg